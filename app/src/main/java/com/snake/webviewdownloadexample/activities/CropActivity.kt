package com.snake.webviewdownloadexample.activities

import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.snake.webviewdownloadexample.R
import com.snake.webviewdownloadexample.croppers.DrawCropView
import com.snake.webviewdownloadexample.databinding.ActivityCropBinding
import com.snake.webviewdownloadexample.utils.Common
import com.snake.webviewdownloadexample.utils.Constant

class CropActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCropBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityCropBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        val originalImage = Common.bitmapResult

        if (originalImage != null) {
            binding.cropView.setImageBitmap(originalImage)
            binding.cropView.setOnCropListener(object : DrawCropView.OnCropListener {
                override fun onCrop(result: Bitmap?) {
                    binding.imageView.bringToFront()
                    binding.cropView.visibility = View.GONE
                    binding.imageView.setImageBitmap(result)
                }
            })
        } else {
            Toast.makeText(this@CropActivity, "Error Image!", Toast.LENGTH_SHORT).show()
            finish()
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        Common.bitmapResult = null
    }

}