package com.snake.webviewdownloadexample.activities

import android.Manifest
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.snake.webviewdownloadexample.R
import com.snake.webviewdownloadexample.databinding.ActivityMainBinding
import com.snake.webviewdownloadexample.utils.Constant
import com.snake.webviewdownloadexample.utils.checkStoragePermission

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        requestStoragePermission()

        binding.edtSearch.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                searchAction()
            }
            return@setOnEditorActionListener false
        }

        binding.btnSearch.setOnClickListener {
            searchAction()
        }

    }

    private fun searchAction() {
        val data = binding.edtSearch.text.toString()
        if (data.isNotEmpty()) {
            val intent = Intent(this@MainActivity, WebViewActivity::class.java)
            intent.putExtra(Constant.SEARCH_DATA_EXTRA, data)
            startActivity(intent)
        } else {
            Toast.makeText(this@MainActivity, "Please do not leave the data field blank!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun requestStoragePermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU) {
            if (!checkStoragePermission()) {
                registerRequestPermission.launch(Manifest.permission.READ_EXTERNAL_STORAGE)
            }
        } else {
            if (!checkStoragePermission()) {
                registerRequestPermission.launch(Manifest.permission.READ_MEDIA_IMAGES)
            }
        }
    }

    private val registerRequestPermission =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { _ ->
        }

}