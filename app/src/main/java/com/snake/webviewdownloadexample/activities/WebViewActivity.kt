package com.snake.webviewdownloadexample.activities

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.ContextMenu
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebResourceRequest
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebView.HitTestResult
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.snake.webviewdownloadexample.databinding.ActivityWebViewBinding
import com.snake.webviewdownloadexample.utils.Common
import com.snake.webviewdownloadexample.utils.Constant
import com.snake.webviewdownloadexample.utils.convert
import com.snake.webviewdownloadexample.utils.gone
import com.snake.webviewdownloadexample.utils.visible
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class WebViewActivity : AppCompatActivity() {

    private lateinit var binding: ActivityWebViewBinding
    private var resultBitmap: Bitmap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityWebViewBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(binding.main) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        onBackPressedDispatcher.addCallback(this, onBackPressedCallback)

        setupSettingWebView()

        binding.webView.setWebViewClient(object : WebViewClient() {
            override fun shouldOverrideUrlLoading(
                view: WebView,
                request: WebResourceRequest
            ): Boolean {
                Log.d("TAG=====", "Link: ${request.url.path}")
                return false
            }

        })


        binding.webView.setWebChromeClient(object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, newProgress: Int) {
                super.onProgressChanged(view, newProgress)
                binding.pbLoading.progress = newProgress
                if (newProgress >= 99) {
                    binding.pbLoading.visibility = View.GONE
                } else {
                    binding.pbLoading.visibility = View.VISIBLE
                }
            }
        })

        val data = intent.getStringExtra(Constant.SEARCH_DATA_EXTRA)
        val googleDomain = "https://google.com/search?tbm=isch&q="
        val url = googleDomain + data
        binding.webView.loadUrl(url)

        binding.viewLoading.setOnClickListener {
            binding.viewLoading.visibility = View.GONE
        }

        binding.tvCancel.setOnClickListener {
            binding.viewLoading.visibility = View.GONE
        }

        binding.tvUse.setOnClickListener {
            binding.viewLoading.visibility = View.GONE
            if (resultBitmap != null) {
                Common.bitmapResult = resultBitmap
                val intent = Intent(this@WebViewActivity, CropActivity::class.java)
//                intent.putExtra(Constant.ORIGINAL_IMAGE_EXTRA, resultBitmap)
                startActivity(intent)
            } else {
                Toast.makeText(this@WebViewActivity, "Error Image!", Toast.LENGTH_SHORT).show()
            }
        }

        registerForContextMenu(binding.webView)

    }

    private fun setupSettingWebView() {
        val settings = binding.webView.settings
        settings.javaScriptEnabled = true
        settings.loadsImagesAutomatically = true
        settings.javaScriptCanOpenWindowsAutomatically = true
        settings.displayZoomControls = true
        settings.loadWithOverviewMode = true
        settings.allowFileAccess = true

        binding.webView.clearFormData()
        settings.defaultTextEncodingName = "UTF-8"
        settings.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
        settings.databaseEnabled = true
        settings.builtInZoomControls = false
        settings.setSupportZoom(true)
        settings.useWideViewPort = true
        settings.domStorageEnabled = true
        settings.blockNetworkImage = false
        settings.blockNetworkLoads = false
    }

    private val onBackPressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            if (binding.webView.canGoBack()) {
                binding.webView.goBack()
            } else {
                finish()
            }
        }
    }

    override fun onCreateContextMenu(
        menu: ContextMenu?,
        v: View?,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        val webViewHitTestResult: HitTestResult = binding.webView.getHitTestResult()
        val downloadImageUrl = webViewHitTestResult.extra
        if (downloadImageUrl != null) {
            if (webViewHitTestResult.type == HitTestResult.IMAGE_TYPE ||
                webViewHitTestResult.type == HitTestResult.SRC_IMAGE_ANCHOR_TYPE
            ) {
                try {
                    Log.d("TAG=====", "Link: $downloadImageUrl")
                    if (downloadImageUrl.startsWith("https://")) {
                        CoroutineScope(Dispatchers.IO).launch {
                            Glide.with(this@WebViewActivity).asBitmap().load(downloadImageUrl).into(object : CustomTarget<Bitmap>() {
                                override fun onResourceReady(
                                    resource: Bitmap,
                                    transition: Transition<in Bitmap>?
                                ) {
                                    resultBitmap = resource
                                    CoroutineScope(Dispatchers.Main).launch {
                                        binding.ivResult.setImageBitmap(resultBitmap)
                                        binding.viewLoading.visible()
                                    }
                                }

                                override fun onLoadCleared(placeholder: Drawable?) {

                                }
                            })
                        }
                    } else {
                        CoroutineScope(Dispatchers.IO).launch {
                            resultBitmap = convert(downloadImageUrl)
                            CoroutineScope(Dispatchers.Main).launch {
                                binding.ivResult.setImageBitmap(resultBitmap)
                                binding.viewLoading.visible()
                            }
                        }
                    }
                } catch (e: Exception) {
                    binding.viewLoading.gone()
                    Log.d("TAG=====", "Error: ${e.message}")
                }
            }
        }
    }

}