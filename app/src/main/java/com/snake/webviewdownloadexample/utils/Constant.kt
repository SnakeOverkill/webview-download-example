package com.snake.webviewdownloadexample.utils

object Constant {

    const val SEARCH_DATA_EXTRA = "search_data_extra"
    const val ORIGINAL_IMAGE_EXTRA = "original_image_extra"

}