package com.snake.webviewdownloadexample.croppers

import android.graphics.Bitmap

object BitmapUtil {
    @JvmStatic
    fun cropBitmapToBoundingBox(picToCrop: Bitmap, unusedSpaceColor: Int): Bitmap {
        val pixels = IntArray(picToCrop.getHeight() * picToCrop.getWidth())
        var marginTop = 0
        var marginBottom = 0
        var marginLeft = 0
        var marginRight = 0
        var i: Int
        picToCrop.getPixels(
            pixels, 0, picToCrop.getWidth(), 0, 0,
            picToCrop.getWidth(), picToCrop.getHeight()
        )
        i = 0
        while (i < pixels.size) {
            if (pixels[i] != unusedSpaceColor) {
                marginTop = i / picToCrop.getWidth()
                break
            }
            i++
        }
        i = 0
        outerLoop1@ while (i < picToCrop.getWidth()) {
            var j = i
            while (j < pixels.size) {
                if (pixels[j] != unusedSpaceColor) {
                    marginLeft = j % picToCrop.getWidth()
                    break@outerLoop1
                }
                j += picToCrop.getWidth()
            }
            i++
        }
        i = pixels.size - 1
        while (i >= 0) {
            if (pixels[i] != unusedSpaceColor) {
                marginBottom = (pixels.size - i) / picToCrop.getWidth()
                break
            }
            i--
        }
        i = pixels.size - 1
        outerLoop2@ while (i >= 0) {
            var j = i
            while (j >= 0) {
                if (pixels[j] != unusedSpaceColor) {
                    marginRight = picToCrop.getWidth() - j % picToCrop.getWidth()
                    break@outerLoop2
                }
                j -= picToCrop.getWidth()
            }
            i--
        }
        return Bitmap.createBitmap(
            picToCrop, marginLeft, marginTop,
            picToCrop.getWidth() - marginLeft - marginRight,
            picToCrop.getHeight() - marginTop - marginBottom
        )
    }
}
