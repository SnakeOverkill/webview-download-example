package com.snake.webviewdownloadexample.croppers

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.CornerPathEffect
import android.graphics.DashPathEffect
import android.graphics.Matrix
import android.graphics.Paint
import android.graphics.Path
import android.graphics.PorterDuff
import android.graphics.PorterDuffXfermode
import android.graphics.Rect
import android.graphics.RectF
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.widget.Toast
import com.snake.webviewdownloadexample.croppers.BitmapUtil.cropBitmapToBoundingBox
import kotlin.math.pow
import kotlin.math.sqrt

class DrawCropView : View, OnTouchListener {
    private var magnifierEnabled = true
    private var SIZE_MAGNIFIER = 200
    private var SIZE_PART_MAGNIFY = 40
    private var DISTANCE_CONSIDER_CLOSER = 100
    private var POSITIONS_MINIMUM = 12
    val lineWithDashPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    val linePaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private var viewCanvasWidth = 0
    private var viewCanvasHeight = 0
    private var targetOriginalBitmap: Bitmap? = null
    private var actualVisibleBitmap: Bitmap? = null
    private val drawCoordinates = ArrayList<Coordinate>()
    private var onCropListener: OnCropListener? = null

    // --- library UserSide ---
    fun setMagnifierSize(SIZE_MAGNIFIER: Int) {
        this.SIZE_MAGNIFIER = SIZE_MAGNIFIER
    }

    fun setMagnifyPartSize(SIZE_PART_MAGNIFY: Int) {
        this.SIZE_PART_MAGNIFY = SIZE_PART_MAGNIFY
    }

    fun setMaginfierEnabled(maginfierEnabled: Boolean) {
        this.magnifierEnabled = maginfierEnabled
    }

    fun setDistanceCloser(DISTANCE_CONSIDER_CLOSER: Int) {
        this.DISTANCE_CONSIDER_CLOSER = DISTANCE_CONSIDER_CLOSER
    }

    fun setMinimumPositions(POSITIONS_MINIMUM: Int) {
        this.POSITIONS_MINIMUM = POSITIONS_MINIMUM
    }

    // --- library UserSide ---
    constructor(context: Context?) : super(context) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init()
    }

    fun init() {
        // construct paint for line (draw path)
        lineWithDashPaint.style = Paint.Style.STROKE
        lineWithDashPaint.setPathEffect(DashPathEffect(floatArrayOf(10f, 20f), 0f))
        lineWithDashPaint.strokeWidth = 5f
        lineWithDashPaint.setShadowLayer(10.0f, 0.0f, 2.0f, -0x1000000)
        lineWithDashPaint.setColor(Color.WHITE)
        linePaint.style = Paint.Style.STROKE
        linePaint.setPathEffect(DashPathEffect(floatArrayOf(10f, 20f), 0f))
        linePaint.strokeWidth = 3f
        linePaint.setShadowLayer(10.0f, 0.0f, 2.0f, -0x1000000)
        linePaint.setColor(Color.WHITE)
        drawCoordinates.clear()
        setOnTouchListener(this)
        invalidate()
    }

    fun setImageBitmap(imageBitmap: Bitmap?) {
        targetOriginalBitmap = imageBitmap
        actualVisibleBitmap = null
        invalidate()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        viewCanvasWidth = canvas.width
        viewCanvasHeight = canvas.height
        if (targetOriginalBitmap != null) {
            // If target's original bitmap is bigger than view size, adjust size for fit
            if (actualVisibleBitmap == null) actualVisibleBitmap = scaleBitmapAndKeepRation(
                targetOriginalBitmap!!, canvas.height, canvas.width
            )
            canvas.drawBitmap(
                actualVisibleBitmap!!,
                (viewCanvasWidth / 2 - actualVisibleBitmap!!.getWidth() / 2).toFloat(),
                (
                        viewCanvasHeight / 2 - actualVisibleBitmap!!.getHeight() / 2).toFloat(),
                null
            )
        } else {
            val textPaint = Paint()
            canvas.drawPaint(textPaint)
            textPaint.setColor(Color.BLACK)
            textPaint.textSize = 16f
            canvas.drawText(
                "Please set image bitmap for process",
                viewCanvasWidth.toFloat(),
                viewCanvasHeight.toFloat(),
                textPaint
            )
        }
        if (drawCoordinates.size > 0) {
            // crop parts for magnify
//            canvas.drawBitmap(getMagnifierPart(drawCoordinates.get(drawCoordinates.size() - 1)), 0, 0, null);
            if (magnifierEnabled) {
                canvas.drawBitmap(
                    Bitmap.createScaledBitmap(
                        getMagnifierPart(
                            drawCoordinates[drawCoordinates.size - 1]
                        ), SIZE_MAGNIFIER, SIZE_MAGNIFIER, false
                    ), 50f, 50f, lineWithDashPaint
                )
                canvas.drawPoint(
                    (SIZE_MAGNIFIER / 2).toFloat(),
                    (SIZE_MAGNIFIER / 2).toFloat(),
                    lineWithDashPaint
                )
            }
            canvas.drawPath(genPathByCoordinate(drawCoordinates, 1), lineWithDashPaint)
        }
    }

    fun getMagnifierPart(touchedPoint: Coordinate): Bitmap {
        // adjust view coordinate to bitmap side coordinate
        val touchedPointOnBitmap = Coordinate()
        touchedPointOnBitmap.x =
            touchedPoint.x - (viewCanvasWidth / 2 - actualVisibleBitmap!!.getWidth() / 2)
        touchedPointOnBitmap.y =
            touchedPoint.y - (viewCanvasHeight / 2 - actualVisibleBitmap!!.getHeight() / 2)
        val conf =
            Bitmap.Config.ARGB_8888 // see other conf types
        val bmp = Bitmap.createBitmap(
            actualVisibleBitmap!!.getWidth() + SIZE_PART_MAGNIFY * 2,
            actualVisibleBitmap!!.getHeight() + SIZE_PART_MAGNIFY * 2, conf
        )
        val canvas = Canvas(bmp)
        canvas.drawColor(Color.BLACK)
        val dst = Rect(
            canvas.width / 2 - actualVisibleBitmap!!.getWidth() / 2,
            canvas.height / 2 - actualVisibleBitmap!!.getHeight() / 2,
            canvas.width / 2 + actualVisibleBitmap!!.getWidth() / 2,
            canvas.height / 2 + actualVisibleBitmap!!.getHeight() / 2
        )
        canvas.drawBitmap(actualVisibleBitmap!!, null, dst, null)
        canvas.drawPath(
            genPathByCoordinate(genCoordinatesForMagnifier(drawCoordinates), 1),
            linePaint
        )
        return Bitmap.createBitmap(
            bmp,
            touchedPointOnBitmap.x.toInt(),
            touchedPointOnBitmap.y.toInt(),
            SIZE_PART_MAGNIFY * 2,
            SIZE_PART_MAGNIFY * 2
        )
    }

    override fun onTouch(v: View, event: MotionEvent): Boolean {
        val currentCoordinate = Coordinate()
        currentCoordinate.x = event.x.toInt().toFloat()
        currentCoordinate.y = event.y.toInt().toFloat()
        adjustCoordinateForFit(currentCoordinate)
        when (event.action) {
            MotionEvent.ACTION_DOWN, MotionEvent.ACTION_MOVE -> {
                drawCoordinates.add(currentCoordinate)
                invalidate()
                if (drawCoordinates.size < 24)
                this.invalidate()
            }

            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                if (measureDistance(
                        drawCoordinates[0],
                        currentCoordinate
                    ) >= DISTANCE_CONSIDER_CLOSER
                ) {
                    Toast.makeText(
                        context,
                        "Please put up your hands in closer with first position",
                        Toast.LENGTH_SHORT
                    ).show()
                    drawCoordinates.clear()
                    this.invalidate()
                }
                if (drawCoordinates.size < POSITIONS_MINIMUM) {
                    Toast.makeText(
                        context,
                        "Please draw more positions for create sticker",
                        Toast.LENGTH_SHORT
                    ).show()
                    drawCoordinates.clear()
                    this.invalidate()
                }
                showProduceDialog()
                this.invalidate()
            }

            else -> {}
        }
        return true
    }

    private fun genPathByCoordinate(coordinates: ArrayList<Coordinate>, interval: Int): Path {
        val path = Path()
        var first = true
        var ci = 0
        while (ci < coordinates.size) {
            var coordinateData = coordinates[ci]
            if (first) {
                first = false
                path.moveTo(coordinateData.x, coordinateData.y)
            } else if (ci < coordinates.size - 1) {
                val next = coordinates[ci + 1]
                path.quadTo(coordinateData.x, coordinateData.y, next.x, next.y)
            } else {
                coordinateData = coordinates[ci]
                path.lineTo(coordinateData.x, coordinateData.y)
            }
            ci += interval
        }
        return path
    }

    private fun measureDistance(targetA: Coordinate, targetB: Coordinate): Int {
        val distance = sqrt((targetA.x - targetB.x).pow(2.0f) + (targetA.y - targetB.y).pow(2.0f))
        return distance.toInt()
    }

    private fun genCoordinatesForMagnifier(coordinates: ArrayList<Coordinate>): ArrayList<Coordinate> {
        val additionCoordinates = ArrayList<Coordinate>()
        for (coordinate in coordinates) {
            var modifiedCoordinate = Coordinate()
            modifiedCoordinate.x = coordinate.x
            modifiedCoordinate.y = coordinate.y
            modifiedCoordinate = convertToBitmapSideCoordinate(modifiedCoordinate)
            modifiedCoordinate.x += SIZE_PART_MAGNIFY.toFloat()
            modifiedCoordinate.y += SIZE_PART_MAGNIFY.toFloat()
            additionCoordinates.add(modifiedCoordinate)
        }
        return additionCoordinates
    }

    private fun adjustCoordinateForFit(targetCoordinate: Coordinate): Coordinate {
        // if selected coordinate is over the actual visible bitmap's area, adjust it.
        val targetWidth = actualVisibleBitmap!!.getWidth()
        val targetHeight = actualVisibleBitmap!!.getHeight()
        val targetStartWidth = (viewCanvasWidth - targetWidth) / 2
        val targetStartHeight = (viewCanvasHeight - targetHeight) / 2
        if (targetCoordinate.x < targetStartWidth) targetCoordinate.x = targetStartWidth.toFloat()
        if (targetCoordinate.x > targetStartWidth + targetWidth) targetCoordinate.x =
            (targetStartWidth + targetWidth).toFloat()
        if (targetCoordinate.y < targetStartHeight) targetCoordinate.y = targetStartHeight.toFloat()
        if (targetCoordinate.y > targetStartHeight + targetHeight) targetCoordinate.y =
            (targetStartHeight + targetHeight).toFloat()
        return targetCoordinate
    }

    // get coordinates on actual visible bitmap side.
    private fun convertToBitmapSideCoordinate(targetCoordinate: Coordinate): Coordinate {
        var targetCoordinate = targetCoordinate
        targetCoordinate = adjustCoordinateForFit(targetCoordinate)
        val bitmapSideCoordinate = Coordinate()
        bitmapSideCoordinate.x =
            targetCoordinate.x - (viewCanvasWidth / 2 - actualVisibleBitmap!!.getWidth() / 2)
        bitmapSideCoordinate.y =
            targetCoordinate.y - (viewCanvasHeight / 2 - actualVisibleBitmap!!.getHeight() / 2)
        return bitmapSideCoordinate
    }

    private fun showProduceDialog() {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Do you proceed like this?")
        builder.setMessage("The image will be generated as you draw.")
        builder.setPositiveButton(
            "Yes"
        ) { dialog: DialogInterface?, which: Int ->
            if (onCropListener != null) onCropListener!!.onCrop(produce())
            init()
        }
        builder.setNegativeButton(
            "No"
        ) { dialog: DialogInterface?, which: Int -> init() }
        builder.show()
    }

    fun setOnCropListener(onCropListener: OnCropListener?) {
        this.onCropListener = onCropListener
    }

    fun produce(): Bitmap {
        val resultImage = Bitmap.createBitmap(
            viewCanvasWidth,
            viewCanvasHeight,
            actualVisibleBitmap!!.getConfig()
        )
        val resultCanvas = Canvas(resultImage)
        val resultPaint = Paint()

        // struct paint for naturally
        resultPaint.isAntiAlias = true
        resultPaint.isDither = true // set the dither to true
        resultPaint.strokeJoin = Paint.Join.ROUND // set the join to round you want
        resultPaint.strokeCap = Paint.Cap.ROUND // set the paint cap to round too
        resultPaint.setPathEffect(CornerPathEffect(10f))
        resultPaint.isAntiAlias = true // set anti alias so it smooths

        // struct paint for path-crop
        resultCanvas.drawPath(genPathByCoordinate(drawCoordinates, 1), resultPaint)
        resultPaint.setXfermode(PorterDuffXfermode(PorterDuff.Mode.SRC_IN))
        val dst = Rect(
            viewCanvasWidth / 2 - actualVisibleBitmap!!.getWidth() / 2,
            viewCanvasHeight / 2 - actualVisibleBitmap!!.getHeight() / 2,
            viewCanvasWidth / 2 + actualVisibleBitmap!!.getWidth() / 2,
            viewCanvasHeight / 2 + actualVisibleBitmap!!.getHeight() / 2
        )
        resultCanvas.drawBitmap(actualVisibleBitmap!!, null, dst, resultPaint)
        return cropBitmapToBoundingBox(resultImage, Color.TRANSPARENT)
    }

    inner class Coordinate {
        var x = 0f
        var y = 0f
    }

    interface OnCropListener {
        fun onCrop(result: Bitmap?)
    }

    companion object {
        fun scaleBitmapAndKeepRation(
            TargetBmp: Bitmap,
            reqHeightInPixels: Int,
            reqWidthInPixels: Int
        ): Bitmap {
            val m = Matrix()
            m.setRectToRect(
                RectF(
                    0f,
                    0f,
                    TargetBmp.getWidth().toFloat(),
                    TargetBmp.getHeight().toFloat()
                ),
                RectF(0f, 0f, reqWidthInPixels.toFloat(), reqHeightInPixels.toFloat()),
                Matrix.ScaleToFit.CENTER
            )
            return Bitmap.createBitmap(
                TargetBmp,
                0,
                0,
                TargetBmp.getWidth(),
                TargetBmp.getHeight(),
                m,
                true
            )
        }
    }
}